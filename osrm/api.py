import asyncio
import os
import json
from aiopg import create_pool
from aiohttp import web, web_exceptions

from osrm import OSRM

POSTGRES_CONN_STRING = os.environ.get(
    'POSTGRES_CONN_STRING',
    'postgresql://postgres:mysecretpassword@localhost:5432/postgres')

loop = asyncio.get_event_loop()
pool = loop.run_until_complete(create_pool(dsn=POSTGRES_CONN_STRING))

_osrm = OSRM()


# export POSTGRES_CONN_STRING and OSRM_HTTP_API
async def air_distance_polygon(lon, lat, radius, num_segments):
    async with pool.acquire() as conn:
        async with conn.cursor() as cursor:
            await cursor.execute(
                "SELECT ST_AsGeoJSON(ST_Buffer(ST_GeographyFromText"
                "('SRID=4326;POINT({0} {1})'), {2}, {3}))".format(
                    lon, lat, radius, num_segments))

            initial_polygon, *_ = await cursor.fetchone()
            return initial_polygon


@asyncio.coroutine
def tasks_caller(coordinates, src_lon, src_lat, distance):
    tasks = set()
    job_number = 0
    for lon, lat in coordinates:
        tasks.add(_osrm.decrease_route(src_lat, src_lon, lat, lon, distance, job_number))
        job_number += 1
    result = yield from asyncio.gather(*tasks, loop=loop)
    return result


async def polygon_by_road_distance(request):
    data = request.GET
    try:
        src_lon = float(data.get('lng'))
        src_lat = float(data.get('lat'))
        distance = float(data.get('distance'))
    except (ValueError, TypeError, KeyError):
        raise web_exceptions.HTTPBadRequest(text='Parameters lng, lat and distance are mandatory')

    num_segments = int(data.get('num_segments', 16))
    result = await air_distance_polygon(src_lon, src_lat, distance, num_segments)
    print('Input: {}'.format(result))

    try:
        poly = json.loads(result)
    except:
        pass

    coordinates = poly['coordinates'][0]
    result = await tasks_caller(coordinates, src_lon, src_lat, distance)
    result = list(map(lambda x: x[1], sorted(result)))

    return web.json_response({'type': 'Polygon', 'coordinates': [result]})


def main(argv):
    app = web.Application(loop=loop)
    app.router.add_get('/polygon', polygon_by_road_distance)

    return web.run_app(app)

if __name__ == '__main__':
    main(None)