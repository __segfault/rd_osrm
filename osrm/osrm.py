import os
import json
import logging
import nvector as nv
from urllib.error import URLError
from urllib import request

logger = logging.getLogger(__name__)

_DEBUG = False
_EARTH_RADIUS = 6371000


class OSRM:
    def __init__(self, http_api=None):
        _http_api = http_api or os.getenv('OSRM_HTTP_API','')
        self.route_url = 'http://' + _http_api +\
            '/route/v1/driving/{0},{1};{2},{3}?alternatives=false&steps=true'

    @staticmethod
    def interpolate_distance(t0_lat, t0_lon, t1_lat, t1_lon, t0, t1, ti, ti_in_m):
        curr_path_calc = prev_path_calc = _EARTH_RADIUS
        lower_deviation = ti_in_m - (ti_in_m * 0.001)
        upper_deviation = ti_in_m + (ti_in_m * 0.001)

        ti_n = (ti - t0) / (t1 - t0)

        logger.debug("ti_in_m: {0}, lower: {1}, upper: {2}".format(ti_in_m, lower_deviation, upper_deviation))
        while round(curr_path_calc, 2) > 0.01 and (curr_path_calc < lower_deviation or curr_path_calc > upper_deviation):
            wgs84 = nv.FrameE(name='WGS84')

            t0_point = wgs84.GeoPoint(latitude=t0_lat, longitude=t0_lon, degrees=True)
            n_EB_E_t0 = t0_point.to_nvector()
            n_EB_E_t1 = wgs84.GeoPoint(latitude=t1_lat, longitude=t1_lon, degrees=True).to_nvector()
            path = nv.GeoPath(n_EB_E_t0, n_EB_E_t1)

            logger.debug('ti normalized:{0}'.format(ti_n))
            g_EB_E_ti = path.interpolate(ti_n).to_geo_point()

            # Optimization over track distance and calculating distance and azimuth assuming spherical object
            # Track distance calculation that was replaced: nv.GeoPath(n_EB_E_t0, g_EB_E_ti).track_distance()
            frame_E = nv.FrameE(a=6371e3, f=0)
            t0_spherical = frame_E.GeoPoint(latitude=t0_lat, longitude=t0_lon, degrees=True)
            ti_spherical = frame_E.GeoPoint(latitude=g_EB_E_ti.latitude_deg[0], longitude=g_EB_E_ti.longitude_deg[0], degrees=True)
            curr_path_calc, *_ = t0_spherical.distance_and_azimuth(ti_spherical)

            logger.debug("ti_n: {0} path_calc: {1}".format(ti_n, curr_path_calc))

            if curr_path_calc > upper_deviation:
                ti_n = ti_n - (ti_n * 0.001)
                if curr_path_calc > prev_path_calc:
                    break # convergence to upper deviation
            else:
                if prev_path_calc != _EARTH_RADIUS and curr_path_calc < prev_path_calc:
                    break # convergence to lower deviation
                ti_n = ti_n + (ti_n * 0.001)

            prev_path_calc = curr_path_calc

        return g_EB_E_ti.longitude_deg[0], g_EB_E_ti.latitude_deg[0]

    async def decrease_route(self, src_lat, src_lon, dest_lat, dest_lon, distance, job_number):
        route = self.get_best_route(src_lat, src_lon, dest_lat, dest_lon)
        if route is None:
            return (job_number, None)

        if float(route['distance']) <= float(distance):
            return (job_number, [dest_lon, dest_lat])

        steps = route['legs'][0]['steps']
        curr_step = len(steps) - 1
        curr_distance = route['distance']
        lon, lat = [0, 0]
        logger.debug('Job # {0}'.format(job_number))
        while (curr_distance > distance):
            curr_distance -= steps[curr_step]['distance']
            if curr_distance == distance:
                lon, lat = steps[curr_step]['maneuver']['location']
                break
            elif curr_distance < distance:
                logger.info(
                    'Current distance: {0}, wanted distance: {1}'.format(curr_distance, distance)
                )

                ti_in_m = distance - curr_distance
                ti = ti_in_m / (steps[curr_step]['distance'] / steps[curr_step]['duration'])
                logger.info('Time interval in meters: {0}, current step index: {1}, \
                            current step distance: {2}'.format(
                            ti_in_m, ti, curr_step, steps[curr_step]['distance']))

                t0_lon, t0_lat = steps[curr_step]['maneuver']['location']
                t1_lon, t1_lat = steps[curr_step + 1]['maneuver']['location']

                lon, lat = self.interpolate_distance(t0_lat, t0_lon, t1_lat, t1_lon, 0,
                                                     steps[curr_step]['duration'], ti, ti_in_m  )
                break
            else:
                curr_step -= 1

        return (job_number, [lon, lat])

    def get_best_route(self, src_lat, src_lon, dest_lat, dest_lon):
        try:
            req = request.urlopen(self.route_url.format(
                                        src_lon, src_lat, dest_lon, dest_lat))
        except URLError as e:
            logger.error('Cannot connect to OSRM server {0}'.format(e))
            return None

        data = req.read()
        try:
            data_decoded = json.loads(str(data, 'utf-8'))
        except (ValueError, KeyError, TypeError) as e:
            logger.error('Invalid JSON {0}'.format(e))
            return None
        return data_decoded['routes'][0]
